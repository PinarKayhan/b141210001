﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210001.Models;
using System.Globalization;

namespace b141210001.Controllers
{
    public class HomeController : Controller
    {
        barınakEntities db = new barınakEntities();
        public ActionResult Index()
        {
            AnaSayfaDTO anaSayfa = new AnaSayfaDTO();
            anaSayfa.sliders = db.slider.ToList();
            anaSayfa.texts = db.text.ToList();
            return View(anaSayfa);

        }

        public ActionResult Ilanlar()
        {
            var liste = db.ilanlar.ToList();
            return View(liste);
        }

        public ActionResult Iletisim()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.bilgiformu = new iletisim();
            return View(obj);
        }

        public ActionResult Ihtiyaclar()
        {
            var liste = db.ihtiyaclar.ToList();
            return View(liste);
        }

        [HttpPost]
        public ActionResult Iletisim(AnaSayfaDTO blgform)
        {
            try
            {
                iletisim _iletisimform = new iletisim();
                _iletisimform.adsoyad = blgform.bilgiformu.adsoyad;
                _iletisimform.mail = blgform.bilgiformu.mail;
                _iletisimform.mesaj = blgform.bilgiformu.mesaj;

                db.iletisim.Add(_iletisimform);
                db.SaveChanges();
                TempData["Mesaj"] = "Form Başarıyla gönderilmiştir.";
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

        public class AnaSayfaDTO
        {
            public List<slider> sliders { get; set; }
            public List<text> texts { get; set; }
            public iletisim bilgiformu { get; set; }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using b141210001.Models;
using System.Globalization;

namespace b141210001.Controllers
{
    public class AdminController : Controller
    {
        barınakEntities db = new barınakEntities();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Iletisim()
        {
            var ileti = db.iletisim.ToList();
            return View(ileti);

        }

        public ActionResult IletisimSil(int IletisimID)
        {
            try
            {
                db.iletisim.Remove(db.iletisim.First(d => d.ID == IletisimID));
                db.SaveChanges();
                return RedirectToAction("Iletisim", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }

        public ActionResult Ilanlar()
        {
            var ilan = db.ilanlar.ToList();
            return View(ilan);

        }
        public ActionResult IlanlarEkle()
        {
            return View();
        }
        public ActionResult IlanlarDuzenle(int IlanlarID)
        {

            var _ilanlarDüzenle = db.ilanlar.Where(x => x.ID == IlanlarID).FirstOrDefault();
            return View(_ilanlarDüzenle);

        }
        public ActionResult IlanlarıSil(int IlanlarID)
        {
            try
            {
                db.ilanlar.Remove(db.ilanlar.First(d => d.ID == IlanlarID));
                db.SaveChanges();
                return RedirectToAction("Ilanlar", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [HttpPost]
        public ActionResult IlanlarEkle(ilanlar t, HttpPostedFileBase file)
        {
            try
            {

                ilanlar _ilanlar = new ilanlar();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _ilanlar.resim = Convert.ToString(memoryStream.ToArray());

                }

                _ilanlar.baslik = t.baslik;
                _ilanlar.acıklama = t.acıklama;

                db.ilanlar.Add(_ilanlar);
                db.SaveChanges();
                return RedirectToAction("Personel", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult IlanlarDuzenle(ilanlar t, HttpPostedFileBase file)
        {
            try
            {

                var _ilanlarDuzenle = db.ilanlar.Where(x => x.ID == t.ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _ilanlarDuzenle.resim = Convert.ToString(memoryStream.ToArray());
                }
                _ilanlarDuzenle.resim = t.resim;
                _ilanlarDuzenle.baslik = t.baslik;
                _ilanlarDuzenle.acıklama = t.acıklama;

                db.SaveChanges();
                return RedirectToAction("Ilanlar", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }


        //Ihtiyaçlar

        public ActionResult Ihtiyaclar()
        {

            var ihtiyac = db.ihtiyaclar.ToList();
            return View(ihtiyac);

        }
        public ActionResult IhtiyaclarEkle()
        {
            return View();
        }
        public ActionResult IhtiyaclarDuzenle(int IhtiyaclarID)
        {

            var _ihtiyaclarDüzenle = db.ihtiyaclar.Where(x => x.ID == IhtiyaclarID).FirstOrDefault();
            return View(_ihtiyaclarDüzenle);

        }
        public ActionResult IhtiyaclarSil(int IhtiyaclarID)
        {
            try
            {
                db.ihtiyaclar.Remove(db.ihtiyaclar.First(d => d.ID == IhtiyaclarID));
                db.SaveChanges();
                return RedirectToAction("Ihtiyaclar", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [HttpPost]
        public ActionResult IhtiyaclarEkle(ilanlar t, HttpPostedFileBase file)
        {
            try
            {

                ihtiyaclar _ihtiyaclar = new ihtiyaclar();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _ihtiyaclar.resim = Convert.ToString(memoryStream.ToArray());

                }

                _ihtiyaclar.baslik = t.baslik;


                db.ihtiyaclar.Add(_ihtiyaclar);
                db.SaveChanges();
                return RedirectToAction("Ihtiyaclar", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult IhtiyaclarDuzenle(ilanlar t, HttpPostedFileBase file)
        {
            try
            {

                var _ihtiyaclarDuzenle = db.ihtiyaclar.Where(x => x.ID == t.ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _ihtiyaclarDuzenle.resim = Convert.ToString(memoryStream.ToArray());
                }
                _ihtiyaclarDuzenle.resim = t.resim;
                _ihtiyaclarDuzenle.baslik = t.baslik;


                db.SaveChanges();
                return RedirectToAction("Ihtiyaclar", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }


        public ActionResult Slider()
        {
            var slider = db.slider.ToList();
            return View(slider);

        }
        public ActionResult SlideEkle()
        {
            return View();
        }
        public ActionResult SlideDuzenle(int SlideID)
        {

            var _slideDuzenle = db.slider.Where(x => x.ID == SlideID).FirstOrDefault();
            return View(_slideDuzenle);

        }
        public ActionResult SlideSil(int SlideID)
        {
            try
            {

                db.slider.Remove(db.slider.First(d => d.ID == SlideID));
                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [HttpPost]
        public ActionResult SlideEkle(slider s, HttpPostedFileBase file)
        {
            try
            {

                slider _slide = new slider();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slide.resim = Convert.ToString(memoryStream.ToArray());
                }

                db.slider.Add(_slide);
                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult SlideDuzenle(slider slide, HttpPostedFileBase file)
        {
            try
            {

                var _slideDuzenle = db.slider.Where(x => x.ID == slide.ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slideDuzenle.resim = Convert.ToString(memoryStream.ToArray());
                }

                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

    }

}